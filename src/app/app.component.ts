import { Component } from '@angular/core';

@Component({
  selector: 'pgd-root',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent {
  title = 'pogodex';
}
